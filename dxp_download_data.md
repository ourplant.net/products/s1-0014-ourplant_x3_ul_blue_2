Here, you will find an overview of the open source informmation of this product. The detailed information can be found within the repository at [GitLab](https://gitlab.com/ourplant.net/products/s1-0014-ourplant_x3_ul_blue_2).

| document | download options |
| -------- | ---------------- |
| operating manual           |[en](https://gitlab.com/ourplant.net/products/s1-0014-ourplant_x3_ul_blue_2/-/raw/main/01_operating_manual/S1-0013-0014_A_OM_OurPlant%20X3_UL.pdf)|
|assembly drawing           |[en](https://gitlab.com/ourplant.net/products/s1-0014-ourplant_x3_ul_blue_2/-/raw/main/02_assembly_drawing/s1-0014-A_ZNB_ourplant_x3_ul_blue2.pdf)|
|circuit diagram            |[en](https://gitlab.com/ourplant.net/products/s1-0014-ourplant_x3_ul_blue_2/-/raw/main/03_circuit_diagram/S1-0013_to_S1-0014_A_EPLAN.pdf)|
|maintenance instructions   |[de](https://gitlab.com/ourplant.net/products/s1-0014-ourplant_x3_ul_blue_2/-/raw/main/04_maintenance_instructions/S1-0013-0014_A_WA_OurPlant%20X3%20UL.pdf), [en](https://gitlab.com/ourplant.net/products/s1-0014-ourplant_x3_ul_blue_2/-/raw/main/04_maintenance_instructions/S1-0013-0014_A_MI_OurPlant%20X3%20UL.pdf)
|spare parts                |[en](https://gitlab.com/ourplant.net/products/s1-0014-ourplant_x3_ul_blue_2/-/raw/main/05_spare_parts/S1-0014_B_SWP_OurPlant-X3-UL.pdf)|

